var helpers = require(global.appRoot + '/entidades/helpers.js');
const bytenode = require('bytenode');
let classSeq = require(global.appRoot + '/app/class/connsequelize.js');
const config = require(global.appRoot + "/config/config.js").link_servicios
const axios = require('axios');
const fs = require("fs");
let qs = require('qs');
let FormData = require('form-data');

const configUrl = require(global.appRoot + '/config/config.js').link_servicios;

let rutaImagen = global.appRoot+"/imagenes/";


function escapeHtml(str) {
    return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;").replace(/`/g, "&#039;").replace(/´/g, "&#039;");
}

var customFunctions = require(global.appRoot + '/app/class/customfunctions.js');

module.exports = async function (req, callback) {
	
    let connSeq = new classSeq(req);
	let model = connSeq.setModel('ejemplo');

    
    console.log("O-".repeat(50))
    console.log("Registro - Mobile")
    console.log("O-".repeat(50))


    let currentdate = new Date(); 
    let Fecha = currentdate.toISOString().substring(0, 10)  + " "  + currentdate.getHours() + ":"  + currentdate.getMinutes() + ":" + currentdate.getSeconds();
    

    //  Variables para dbo.logs
	let Ruta = req.route.path;
    let token = req.headers.token;
    let idUsuario = customFunctions.idUsuario(token);
	let Metodo = JSON.stringify(req.route.methods);
    let Log;
    let logMsg;


    //Variables Propias
    let query;
    let nombre = escapeHtml(req.body.nombre);
    let apellido = escapeHtml(req.body.apellido);
    let celular = escapeHtml(req.body.celular);
    let email = escapeHtml(req.body.email.toLowerCase());
    let dni = escapeHtml(req.body.nDni);
    let fNacimiento = escapeHtml(req.body.fNacimiento);
    let numLicencia = escapeHtml(req.body.numLicencia);
    let emicionLicencia = escapeHtml(req.body.emicionLicencia);
    let vencimientoLicencia = escapeHtml(req.body.vencimientoLicencia);
    let tipoLicencia = escapeHtml(req.body.tipoLicencia.toUpperCase());
    let password = escapeHtml(req.body.password);

    let Datos
    let nombreImagenPerfil;
    let imgVF;

    //  Si trae un Base64 (webcam) usa eso como img.
    //  Sino, (texto del transaccional) usa la img del transaccional

    if(req.body.nombreImagenPerfil.includes('data:image')){
        Datos = JSON.stringify({"nombre":nombre, "apellido":apellido, "celular":celular, "email":email, "dni":dni, "fNacimiento":fNacimiento, "numLicencia":numLicencia, "emicionLicencia":emicionLicencia, "vencimientoLicencia":vencimientoLicencia, "tipoLicencia":tipoLicencia, "password":password});
        nombreImagenPerfil = req.body.nombreImagenPerfil.replace(/^data:image\/\w+;base64,/, "");
    }else{
        nombreImagenPerfil = escapeHtml(req.body.nombreImagenPerfil);
        Datos = req.body
    }

    //  Si trae un Base64 (webcam) usa eso como img.
    //  Sino, ( Sin Imagen by Totem ) texto unicamente para mobile

    if(req.body.imgVF.includes('data:image')){
        imgVF = req.body.imgVF.replace(/^data:image\/\w+;base64,/, "");
    }else{
        imgVF = req.body.imgVF
    }



    let nombreImagenDni = escapeHtml(req.body.nombreImagenDni);
    let nombreImagenLicencia = escapeHtml(req.body.nombreImagenLicencia);

    let axiosCrearUsuario;
    let datosUsuarioNitro4;
    let usuarioExiste;
    let nuevoToken;
   
    // console.log("req.body -> ",req.body)
    // console.log("req.headers -> ",req.headers)

    if(token == undefined || token == ''){
        console.log("Error de Token")
        callback(helpers.respuestaOk({
            "mensaje" : 'El Token es requerido'
        }));    
        return
    }

    // Verificamos si el usuario ya existe
    try {
    
        query = `SELECT TOP 1 * FROM dbo.datos_personales WHERE DNI='${dni}'`;
        // console.log(query)
        usuarioExiste = await model.query(connSeq.getConn(),query).then(function(resultados) {
            return resultados
        }).catch(function(error){
            console.log("Error de Verificar")
            // callback(helpers.respuestaError(`Error al solicitar usuario con DNI: ${dni}`));
        });


        query = `SELECT TOP 1 * FROM dbo.datos_personales WHERE Email='${email}'`;
        // console.log(query)
        emailExiste = await model.query(connSeq.getConn(),query).then(function(resultados) {
            return resultados
        }).catch(function(error){
            console.log("Error de Verificar")
            // callback(helpers.respuestaError(`Error al solicitar usuario con DNI: ${dni}`));
        });

    } catch (error) {
        console.log("Error TRY Select dbo.datos_personales")
        console.log(error)
    }
    

    // Verificamos si el usuario ya existe
    if(usuarioExiste.length == 1){
        console.log("Error de Verificar")
        callback(helpers.respuestaOk({
            "mensaje" : `El usuario con DNI: ${dni}, ya esta en uso`
        }));
        return
    }

    if(emailExiste.length == 1){
        console.log("Error de Verificar")
        callback(helpers.respuestaOk({
            "mensaje" : `El email: ${email} , ya esta en uso`
        }));
        return
    }

    //  Creamos la imagen a partir del Base64

    try {
    
        if(req.body.nombreImagenPerfil.includes('data:image')){
            //  Creamos la imagen a partir del Base64

            if (!fs.existsSync(rutaImagen)){
                fs.mkdirSync(rutaImagen);
            }

            fs.writeFileSync(`${rutaImagen}regPerfil.png`, nombreImagenPerfil, 'base64', function(err){
                console.log(err);
            });

            let imagenPerfil = fs.readFileSync(`${rutaImagen}regPerfil.png`);

            // console.log("imagenPerfil -> ",imagenPerfil)
            

            var dataImgPerfil = new FormData();
            dataImgPerfil.append('file', fs.createReadStream(`${rutaImagen}regPerfil.png`));

            var configImgPerfil = {
            method: 'post',
            url: `${configUrl.url_transaccional}/api/files/upload`,
            headers: { 
                'Accept': 'application/json', 
                'Authorization': 'Basic bGlvbnM6SnBoMTM1', 
                'Token': token,
                ...dataImgPerfil.getHeaders()
            },
            data : dataImgPerfil
            };

            await axios(configImgPerfil)
            .then(function (response) {
                nombreImagenPerfil = response.data.filename;
            })
            .catch(function (error) {
                console.log("Error AXIOS Transaccional")
                console.log(error);
            });
        }

    } catch (error) {
        console.log("Error TRY nombreImagenPerfil -> ")
        console.log(error)
    }


    // CREAMOS LA IMAGEN DE VF VISUAL FACE

    try {
        
        if(req.body.imgVF){
            // CREAMOS LA IMAGEN DE VF VISUAL FACE

            if (!fs.existsSync(rutaImagen)){
                fs.mkdirSync(rutaImagen);
            }

            //  Creamos la imagen a partir del Base64

            fs.writeFileSync(`${rutaImagen}regPerfil_VF.png`, imgVF, 'base64', function(err){
                console.log(err);
            });

            let imagenPerfilVF = fs.readFileSync(`${rutaImagen}regPerfil_VF.png`);

            // console.log("imagenPerfilVF -> ",imagenPerfilVF)
            

            var dataImgVF = new FormData();
            dataImgVF.append('file', fs.createReadStream(`${rutaImagen}regPerfil_VF.png`));

            var configImgVF = {
            method: 'post',
            url: `${configUrl.url_transaccional}/api/files/upload`,
            headers: { 
                'Accept': 'application/json', 
                'Authorization': 'Basic bGlvbnM6SnBoMTM1',
                'Token': token,
                ...dataImgVF.getHeaders()
            },
            data : dataImgVF
            };

            await axios(configImgVF)
            .then(function (response) {
                imgVF = response.data.filename;
            })
            .catch(function (error) {
                console.log("Error AXIOS Transaccional")
                console.log(error);
            });

        }
    
    } catch (error) {
        console.log("Error TRY imgVF -> ")
        console.log(error)
    }


    // Si NO existe
    // Creamos el usuario en la tabla master.usuarios (usuario Nitro4)

    try {

        //  Creamos un Log

        logMsg = "Creacion de Nuevo Usuario"
        query = "INSERT INTO dbo.logs (idUsuarioNitro4Modificacion, idUsuarioNitro4Alta, Fecha, Ruta, Metodo, Datos, Usuario, Respuesta) VALUES ("+idUsuario+", "+idUsuario+", '"+Fecha+"', '"+Ruta+"', '"+Metodo+"', '"+Datos+"', "+idUsuario+", '"+logMsg+"')";
        await model.query(connSeq.getConn(),query).then(function(resultados) {
        }).catch(function(error) {
            console.log("ERROR -> ",error)
            // callback(helpers.respuestaError("No se encontraron los elementos"));
            return
        });
        logMsg = ""

        query = "SELECT IDENT_CURRENT('dbo.logs') AS LastId";
        Log = await model.query(connSeq.getConn(),query).then(function(resultados) {
            return resultados[0]['LastId'];
        }).catch(function(error) {
            console.log("ERROR -> ",error)
        // callback(helpers.respuestaError("No se encontraron los elementos"));
        return
        });



        console.log("O-".repeat(50))
        console.log("CREACION USUARIO NITRO4")
        console.log("O-".repeat(50))

        let data = qs.stringify({
            'idUsuario': '',
            'nombre_usuario': nombre,
            'correo_usuario': email,
            });
        let configaxios = {
            method: 'post',
            url: `${configUrl.url_nitro4}/api/usuarios-nitro4/`,
            headers: { 
                'Authorization': 'bGlvbnM6SnBoMTM1', 
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Token': token
        },
            data : data
        };

        axiosCrearUsuario = await axios(configaxios)
            .then(function (response) {
                return response
            })
            .catch(function (error) {
                console.log("Error -> AxiosCrear")
                console.log(error);
            });


    } catch (error) {
        console.log("Error TRY AXIOS -> ",error)
    }



    query = `SELECT TOP 1 * FROM master.usuarios WHERE nombre_usuario = '${nombre}' AND correo_usuario = '${email}' `;
    console.log(query)
    datosUsuarioNitro4 = await model.query(connSeq.getConn(),query).then(function(resultados) {
        return resultados[0]
    }).catch(function(error){
        console.log("Error Select master.usuarios")
        // callback(helpers.respuestaError(`Error al solicitar usuario con DNI: ${dni}`));
    });

    
    
    // console.log("datosUsuarioNitro4 -> ", datosUsuarioNitro4)
    
    
    // Cambio de clave Usuario Nitro4
    try {

        console.log("O-".repeat(50))
        console.log("Cambio de clave Usuario Nitro4")
        console.log("O-".repeat(50))

        let data = qs.stringify({
            'idUsuario': datosUsuarioNitro4.idUsuario,
            'clave': password 
        });
        let configaxios = {
            method: 'post',
            url: `${configUrl.url_nitro4}/api/usuarios-nitro4/modificarClave`,
            headers: { 
                'token': token, 
                'Authorization': 'Basic bGlvbnM6SnBoMTM1', 
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data : data
        };
        
        await axios(configaxios)
        .then(function (response) {
            // console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
            console.log("Error Axios")
            console.log(error);
        });


    } catch (error) {
        console.log("Error-> cambio Clave")
        console.log(error)
    }
    

    // Creacion de usuario en la tabla datos_personales

    console.log("O-".repeat(50))
    console.log("Creacion de usuario en la tabla datos_personales")
    console.log("O-".repeat(50))


    //  Creamos un Log

    logMsg = "Creacion de Usuario en datos_personas"
    query = "INSERT INTO dbo.logs (idUsuarioNitro4Modificacion, idUsuarioNitro4Alta, Fecha, Ruta, Metodo, Datos, Usuario, Respuesta) VALUES ("+idUsuario+", "+idUsuario+", '"+Fecha+"', '"+Ruta+"', '"+Metodo+"', '"+Datos+"', "+idUsuario+", '"+logMsg+"')";
    await model.query(connSeq.getConn(),query).then(function(resultados) {
    }).catch(function(error) {
        console.log("ERROR -> ",error)
        // callback(helpers.respuestaError("No se encontraron los elementos"));
        return
    });
    logMsg = ""

    query = "SELECT IDENT_CURRENT('dbo.logs') AS LastId";
    Log = await model.query(connSeq.getConn(),query).then(function(resultados) {
        return resultados[0]['LastId'];
    }).catch(function(error) {
        console.log("ERROR -> ",error)
    // callback(helpers.respuestaError("No se encontraron los elementos"));
    return
    });

    query = `SELECT TOP 1 * FROM dbo.datos_personales`;
    console.log(query)
    await model.query(connSeq.getConn(),query).then(function(resultados) {
        console.log("Resultados del SELECT DE PRUEBA",resultados)
    }).catch(function(error){
        console.log("Y ahora Rompe aca?")
        // callback(helpers.respuestaError(`Error al solicitar usuario con DNI: ${dni}`));
        return
    });
    
    // query = `INSERT INTO dbo.datos_personales (idUsuarioNitro4Modificacion, idUsuarioNitro4Alta, idUsuarioNitro4Baja, fechaAltaNitro4, Nombre, Apellido, Celular, Email, DNI, Fecha_Nacimiento, Num_Licencia, Licencia_Emision, Licencia_Vencimiento, Licencia_Tipo, Id_Usuario_Nitro4, Img_Persona, Img_VF ) VALUES (1, 1, 1, '${Fecha}', '${nombre}', '${apellido}', '${celular}', '${email}', '${dni}', '${fNacimiento}', '${numLicencia}', '${emicionLicencia}', '${vencimientoLicencia}', '${tipoLicencia}', ${datosUsuarioNitro4.idUsuario}, '${nombreImagenPerfil}', '${imgVF}')`;
    query = `INSERT INTO dbo.datos_personales (idUsuarioNitro4Modificacion, idUsuarioNitro4Alta, idUsuarioNitro4Baja, fechaAltaNitro4, Nombre, Apellido, Celular, Email, DNI, Fecha_Nacimiento, Num_Licencia, Licencia_Emision, Licencia_Vencimiento, Licencia_Tipo, Id_Usuario_Nitro4, Img_dni, Img_Licencia, Img_Persona, Img_VF ) VALUES (1, 1, 1, '${Fecha}', '${nombre}', '${apellido}', '${celular}', '${email}', '${dni}', '${fNacimiento}', '${numLicencia}', '${emicionLicencia}', '${vencimientoLicencia}', '${tipoLicencia}', ${datosUsuarioNitro4.idUsuario}, '${nombreImagenDni}', '${nombreImagenLicencia}', '${nombreImagenPerfil}', '${imgVF}')`;
    console.log(query)
    await model.query(connSeq.getConn(),query).then(function(resultados) {
        console.log(resultados)
    }).catch(function(error){
        console.log("ACA????")
        // callback(helpers.respuestaError(`Error al solicitar usuario con DNI: ${dni}`));
        return
    });



    // OBTENENOS EL NUEVO TOKEN LOGEANDO AL USUARIO
    try {

        console.log("O-".repeat(50))
        console.log("OBTENENOS EL NUEVO TOKEN LOGEANDO AL USUARIO")
        console.log("O-".repeat(50))


        
        let data = qs.stringify({
            'idEmpresa': '4',
            'usuario': `${email}`,
            'clave': `${password}`,
            'pantalla_nitro4': 'true' 
          });
          let configaxios = {
            method: 'post',
            url: `${configUrl.url_nitro4}/api/login-nitro4`,
            headers: { 
              'Authorization': 'bGlvbnM6SnBoMTM1',
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data : data
          };
          
          await axios(configaxios)
          .then(function (response) {
            nuevoToken = response.data.token;
            console.log("Usuario logeado!!")
          })
          .catch(function (error) {
            console.log("Error loguear Usuario")
            console.log(error);
          });    
          
          
    } catch (error) {
        console.log("Error CATCH loguear Usuario")
        console.log(error)
    }





    
    
    console.log(Datos);
    console.log("Callback!");

    callback(helpers.respuestaOk({
        "resultados" : `Usuario "${nombre}", se ah creado de manera correcta!`,
        "token": nuevoToken
    }));


};